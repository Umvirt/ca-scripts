# UmVirt Certficate Authority Infrastructure Scripts

## Key creation

Key with password

```
./key_create example.com
```

Key without password (http)

```
./key_create_nopass example.com
```

## Certificate signing request (CSR) creation

```
./csr_create example.com
```

## Certificate creation

Default cert (one domain)

```
./cert_create example.com
```

SAN cert (fixed list of domains)

```
./sancert_create example.com DNS:example.com,DNS:www.example.com
```

*ToDo* : Wildcard cert (all posible subdomains)

## Certificate revoke

revoke by csr

```
./cert_revoke example.com
```

revoke by id

```
./cert_revokebyid 1014
```

## Certificate info

Certificate info

```
./cert_info example.com
```

CA certificate info

```
./ca_info
```

RootCA certificate info

```
./rootca_info
```

## Certificate match with key

```
./cert_match example.com
```

## Certificate reoke lists (crl)

CRL update

```
./crl_update
```

Push CRL to site (cals crl_update)

```
./push_crl
```




